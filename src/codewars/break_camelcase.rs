// https://www.codewars.com/kata/5208f99aee097e6552000148/train/rust
#[allow(dead_code)]
fn solution(s: &str) -> String {
    let mut result = String::new();
    for c in s.chars() {
        if c.is_uppercase() {
            result.push(' ');
        }
        result.push(c);
    }

    result
}

#[test]
fn test_happy() {
    assert_eq!(solution("camelCasing"), "camel Casing");
    assert_eq!(solution("camelCasingTest"), "camel Casing Test");
    assert_eq!(solution("thisIsASolution"), "this Is A Solution");
}
