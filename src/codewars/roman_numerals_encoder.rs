use std::collections::HashMap;

#[allow(dead_code)]
fn get_symbols() -> HashMap<u32, &'static str> {
    vec![
        (1, "I"),
        (2, "II"),
        (3, "III"),
        (4, "IV"),
        (5, "V"),
        (6, "VI"),
        (7, "VII"),
        (8, "VIII"),
        (9, "IX"),
        (10, "X"),
        (20, "XX"),
        (30, "XXX"),
        (40, "XL"),
        (50, "L"),
        (60, "LX"),
        (70, "LXX"),
        (80, "LXXX"),
        (90, "XC"),
        (100, "C"),
        (200, "CC"),
        (300, "CCC"),
        (400, "CD"),
        (500, "D"),
        (600, "DC"),
        (700, "DCC"),
        (800, "DCCC"),
        (900, "CM"),
        (1000, "M"),
        (2000, "MM"),
        (3000, "MMM"),
    ]
    .into_iter()
    .collect()
}

/// Converts a number to a string representating roman numeral.
/// https://www.codewars.com/kata/51b62bf6a9c58071c600001b
#[allow(dead_code)]
pub fn num_as_roman(num: i32) -> String {
    assert!(num > 0, "Number must be positive");
    let symbols = get_symbols();

    let mut builder = String::new();
    let digits_from_smallest: Vec<u32> = num
        .to_string()
        .chars()
        .rev()
        .enumerate()
        .map(|(place, digit)| digit.to_digit(10).unwrap() * 10u32.pow(place as u32))
        .collect();
    for digit in digits_from_smallest.into_iter().rev().filter(|d| d != &0) {
        let roman = symbols
            .get(&digit)
            .expect(format!("Invalid digit: {}", digit).as_str());
        builder.push_str(roman);
    }
    builder
}

#[test]
fn test_roman() {
    assert_eq!(num_as_roman(182), "CLXXXII");
    assert_eq!(num_as_roman(1990), "MCMXC");
    assert_eq!(num_as_roman(1875), "MDCCCLXXV");
}

#[test]
#[should_panic]
fn test_panic_zero() {
    num_as_roman(0);
}

#[test]
#[should_panic]
fn test_panic_negative() {
    num_as_roman(-13);
}

#[test]
#[should_panic]
fn test_panic_too_large() {
    num_as_roman(10003);
}
