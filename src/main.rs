mod codewars;

fn main() {
    println!(
        "This project has no main activity. \
        You can run test against solutions with 'cargo test'"
    )
}
