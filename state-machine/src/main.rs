use std::cmp::Ordering;
use std::io::{stdin, stdout, Write};
use std::num::ParseIntError;

use rand::{thread_rng, Rng};

fn main() {
    let mut state: Box<dyn State> = Box::new(WelcomeState);
    loop {
        match state.tick() {
            StateTickResult::Change(new_state) => state = new_state,
            StateTickResult::NoChange => (),
            StateTickResult::Finish => break,
        }
    }
    wait_for_enter("Press Enter to exit");
}

enum StateTickResult {
    NoChange,
    Change(Box<dyn State>),
    Finish,
}

trait State {
    fn tick(&mut self) -> StateTickResult;
}

struct WelcomeState;

impl State for WelcomeState {
    fn tick(&mut self) -> StateTickResult {
        println!(
            "
Welcome to number guess example with state machine!
This program written with Rust
The game is simple: the program guesses a number from 0 to 99.
Then you write your guess and program responses with less or greater.
If your number = a guessed one, you win!"
        );
        StateTickResult::Change(Box::new(GenerateNumberState))
    }
}

struct GenerateNumberState;

impl State for GenerateNumberState {
    fn tick(&mut self) -> StateTickResult {
        let random_number = thread_rng().gen_range(0..=99);
        return StateTickResult::Change(Box::new(GuessNumberState::new(random_number)));
    }
}

struct GuessNumberState {
    number: i32,
}

impl GuessNumberState {
    fn new(number: i32) -> Self {
        GuessNumberState { number }
    }
}

impl State for GuessNumberState {
    fn tick(&mut self) -> StateTickResult {
        let input = read_i32("Your number: ").unwrap();
        match self.number.cmp(&input) {
            Ordering::Less => {
                println!("Lesser");
                StateTickResult::NoChange
            }
            Ordering::Greater => {
                println!("Greater");
                StateTickResult::NoChange
            }
            Ordering::Equal => StateTickResult::Change(Box::new(WonState)),
        }
    }
}

struct WonState;

impl State for WonState {
    fn tick(&mut self) -> StateTickResult {
        println!("You won!");
        StateTickResult::Finish
    }
}

fn input(message: &str) -> String {
    print!("{}", message);
    stdout().flush().unwrap();
    let mut ret = String::new();
    stdin()
        .read_line(&mut ret)
        .expect("Failed to read from stdin");
    ret
}

fn wait_for_enter(message: &str) {
    input(message);
}

fn read_i32(prompt: &str) -> Result<i32, ParseIntError> {
    input(prompt).trim().parse()
}
